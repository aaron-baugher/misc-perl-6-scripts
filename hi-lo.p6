#!/usr/bin/env perl6
use v6;
    
my %v = ( 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, ### 1
          7 => 7, 8 => 8, 9 => 9, T => 10,
          J => 11, Q => 12, K => 13, A => 14 );

my @deck := (%v.keys X~ <♠ ♡ ♢ ♣>).pick(5);            ### 2
my $card = @deck.shift;
my $show = "$card ";

for @deck -> $new {                               ### 3
    say $show;
    my $l; repeat {                               ### 4
        $l = prompt 'Hi or Lo? ';
    } until $l ~~ m:i/ ( h | l ) /;               ### 5
    $show ~= "$new ";
    my $nv = %v{ $new.substr(0,1)};               ### 6
    my $cv = %v{$card.substr(0,1)};
    if $nv == $cv or
        ( $nv < $cv and $l  ~~ m/:i h/ ) or       ### 7
        ( $nv > $cv and $l !~~ m/:i h/ ) {
            say $show;
            say "Sorry, you lose!";
            exit;
        }
    $card = $new;
}
say $show;
say 'You win!';


