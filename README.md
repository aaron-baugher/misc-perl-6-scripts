This is a collection of disconnected Perl 6 scripts which I'm developing
as samples while learning the language.  See
http://blogs.perl.org/users/aaron_baugher for articles about them.

